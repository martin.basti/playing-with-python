#! /usr/bin/env python3
"""
Example of using aiohttp with asyncio

Great tutorial:
https://pawelmhm.github.io/asyncio/python/aiohttp/2016/04/22/asyncio-aiohttp.html
"""

import asyncio

from aiohttp import ClientSession
from aiohttp.client_exceptions import ClientConnectionError, ServerTimeoutError


URLS_FILE = 'urls.txt'
TIMEOUT = 10
CONN_LIMIT = 50


async def get_http_head(url, session, sem):
    async with sem:
        try:
            async with session.head(
                url, verify_ssl=False, timeout=TIMEOUT
            ) as response:
                response = await response.read()
        except (ClientConnectionError, asyncio.TimeoutError) as e:
            return url, e
        else:
            return url, response


async def run(urls):
    sem = asyncio.Semaphore(CONN_LIMIT)
    async with ClientSession() as session:
        tasks = (
            asyncio.ensure_future(get_http_head(url, session, sem))
            for url in urls
        )

        return await asyncio.gather(*tasks)


def main():
    with open(URLS_FILE, 'r') as f:
        urls = (url.strip() for url in f.readlines())

    loop = asyncio.get_event_loop()

    results = loop.run_until_complete(run(urls))
    loop.close()

    for url, head in results:
        print("{0}:\t{1}".format(url, head))


if __name__ == '__main__':
    main()
