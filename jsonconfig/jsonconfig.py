
"""
Parsing and validating JSON files using jsonschema module

Useful links for JSON schema format:
- https://spacetelescope.github.io/understanding-json-schema/index.html
- https://python-jsonschema.readthedocs.io
"""

from pprint import pprint
from jsonschema import Draft4Validator, validators


def extend_with_default(validator_class):
    """
    Extend schema to allow default value
    Taken from: https://python-jsonschema.readthedocs.io/en/latest/faq/#why-doesn-t-my-schema-that-has-a-default-property-actually-set-the-default-on-my-instance
    """
    validate_properties = validator_class.VALIDATORS["properties"]

    def set_defaults(validator, properties, instance, schema):
        for property_, subschema in properties.items():
            if "default" in subschema:
                instance.setdefault(property_, subschema["default"])

        for error in validate_properties(
            validator, properties, instance, schema,
        ):
            yield error

    return validators.extend(
        validator_class, {"properties": set_defaults},
    )


DefaultValidatingDraft4Validator = extend_with_default(Draft4Validator)


class JSONSchemaObject(object):

    def __init__(self, name, default=True, additional_properties=False):
        """
        Create a new JSON schema object
        :param name: name of schema
        :param default: create empty object
        :param additional_properties: allow properties that are not defined
               by schema
        """
        self._name = name
        self._schema = {
            'type': 'object',
            'properties': {},
            'additionalProperties': additional_properties,
        }
        if default:
            self._schema['default'] = {}

    @property
    def schema(self):
        return self._schema

    @property
    def name(self):
        return self._name

    def _validate_enum(self, enum, type_):
        if not isinstance(enum, list):
            raise TypeError("option 'enum' must be list")
        if not all(isinstance(item, type_) for item in enum):
            raise TypeError("option 'enum' must contain only '{}'".format(
                type_))

    def _set_required(self, name):
        schema_req = self._schema.setdefault('required', [])
        if name not in schema_req:
            schema_req.append(name)

    def add_string_property(
            self, name, enum=None, description=None, default=None,
            required=False
    ):
        # TODO what if default can be null ?
        inner_dict = {'type': 'string'}

        if enum is not None:
            self._validate_enum(enum, str)
            inner_dict['enum'] = enum

        if default is not None:
            if not isinstance(default, str):
                raise TypeError("option 'default' must be string")
            inner_dict['default'] = default

        if description is not None:
            if not isinstance(description, str):
                raise TypeError("options 'description' must be string")
            inner_dict['description'] = description

        if required:
            self._set_required(name)

        self._schema['properties'][name] = inner_dict

    def add_object_property(self, schema_object, required=False):
        if not isinstance(schema_object, JSONSchemaObject):
            raise TypeError('required JSONSchemaObject')
        self._schema['properties'][schema_object.name] = schema_object.schema
        if required:
            self._set_required(schema_object.name)


LOGGING_SCHEMA = {
    "type": "object",
    "properties": {
        "level": {
            'type': 'string',
            "enum": ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
            "description": "Logging level",
            "default": "INFO",
        },
    },
    "default": {},  # must create empty dict to apply defaults
    'additionalProperties': False,
}

DATABASE_SCHEMA = {
    "type": "object",
    "properties": {
        "driver": {
            'type': 'string',
            'default': 'postgres'
        },
        "username": {'type': 'string'},
        "password": {'type': 'string'},
        "database": {
            'type': 'string',
            'default': 'testdb'
        },
    },
    'required': ['username', 'password'],
    'default': {},
    'additionalProperties': False,
}

TEST_SCHEMA = JSONSchemaObject('testconfig')
TEST_SCHEMA.add_string_property('weekend', enum=['SA', 'SU'], required=True)
TEST_SCHEMA.add_string_property('start_day', default='MO')

DATE_SCHEMA = JSONSchemaObject('date')
DATE_SCHEMA.add_string_property('year', default='2017')
DATE_SCHEMA.add_string_property('month', default='05')

TEST_SCHEMA.add_object_property(DATE_SCHEMA)


APP_SCHEMA = {
    'properties': {
        'logging': LOGGING_SCHEMA,
        'db_settings': DATABASE_SCHEMA,
        TEST_SCHEMA.name: TEST_SCHEMA.schema
    },
    'additionalProperties': False,
}


def main():
    obj = {
        'db_settings': {
            'password': 'test',
            'username': 'test',
        },
        'testconfig': {
            'weekend': 'SA',
        }
    }
    DefaultValidatingDraft4Validator(APP_SCHEMA).validate(obj)
    pprint(obj)

if __name__ == '__main__':
    main()
