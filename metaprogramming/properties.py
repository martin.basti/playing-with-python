
"""
Dynamic creation fo properties from attributes
"""


def cool_property(name, allowed_types=(str,)):
    underscore_name = f'_{name}'

    assert isinstance(allowed_types, tuple)
    if not allowed_types:
        raise ValueError('allowed_types cannot be empty')

    @property
    def p(self):
        return getattr(self, underscore_name)

    @p.setter
    def p(self, value):
        if not isinstance(value, allowed_types):
            raise TypeError(
                f'{name} expected {allowed_types} got {type(value)}')
        setattr(self, underscore_name, value)

    return p


class Car:
    max_speed = cool_property('max_speed', (int, float))
    color = cool_property('color', (str,))


if __name__ == '__main__':
    c = Car()
    print(dir(c))
    c.max_speed = 230
    c.max_speed = 230.0
    c.color = 'blue'
    print(c.color)
    c.color = 20
